Author: Austin Mohler

Contact: amohler@uoregon.edu

Description: Simple web server built in python taking in URL's and outputting proper http responses and error codes.

Credits: Zane Globus-O'Harra (Helped me with some trivial git commands)

Issues: 3/4 test cases work. Surpisingly the first test case does not work, but the final three do work. I did put the url into a browser and continued to have the same issues. I tried my best to fix this, but I was hoping I could get a bit of an explanation sometime on where I went wrong. Using the "os" library to a fuller extent was super new to me and I sense that's where I went wrong. Thanks!